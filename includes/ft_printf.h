/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/07 18:57:57 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/07 18:58:07 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include "../libft/libft.h"
# include <stdarg.h>
# include <limits.h>
# include <stdio.h>
# include <stdlib.h>
# include <stdint.h>
# include <unistd.h>

# define Z 1
# define M 2
# define A1 4
# define A2 8
# define P 16

typedef struct	s_printf
{
	short		f;
	int			w;
	int			p;
	char		*frmt;
	va_list		arg;
	char		conv;
	int			ret;
	int			len;
}				t_printf;

int				ft_printf(const char *frmt, ...);

void			flags_init(t_printf *l);
void			check_flags(t_printf *l);
void			check_convert(t_printf *l);
void			parsing(t_printf *l);

void			print_c(t_printf *l);
void			print_s(t_printf *l);
void			print_p(t_printf *l);
void			print_i(t_printf *l);
void			print_d(t_printf *l);
void			print_u(t_printf *l);
void			print_x(t_printf *l, int up);
void			print_per(t_printf *l);

#endif
