/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/03 15:46:08 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/22 14:17:17 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static void	process_length2(t_printf *l)
{
	if (l->f & A1)
	{
		if (l->f & A2)
		{
			l->w = va_arg(l->arg, int);
			l->p = va_arg(l->arg, int);
		}
		else
		{
			if (*(l->frmt - 1) == '*')
				l->p = va_arg(l->arg, int);
			else
				l->w = va_arg(l->arg, int);
		}
		l->f &= ~A1;
	}
	while (ft_isdigit(*l->frmt))
	{
		l->p *= 10;
		l->p += (*l->frmt - 48);
		l->frmt++;
	}
}

static void	process_length(t_printf *l)
{
	if (!(l->f & P))
	{
		if (l->f & A1)
			l->w = va_arg(l->arg, int);
		while (ft_isdigit(*l->frmt))
		{
			l->w *= 10;
			l->w += (*l->frmt - 48);
			l->frmt++;
		}
	}
	else
		process_length2(l);
}

static void	process_precision(t_printf *l)
{
	if (l->f & P)
	{
		if (l->f & A1)
			l->p = va_arg(l->arg, int);
		while (ft_isdigit(*l->frmt))
		{
			l->p *= 10;
			l->p += (*l->frmt - 48);
			l->frmt++;
		}
	}
	if (l->w < 0)
	{
		l->w *= -1;
		if (l->f & Z)
			l->f &= ~Z;
		if (!(l->f & M))
			l->f |= M;
	}
	if (l->p < 0)
	{
		l->p = 0;
		l->f &= ~P;
	}
}

void		parsing(t_printf *l)
{
	check_flags(l);
	process_length(l);
	check_flags(l);
	process_precision(l);
	check_convert(l);
}
