/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_u.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 14:58:33 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/27 15:43:25 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static void			print_minus_u(t_printf *l, unsigned int u)
{
	if (l->f & M && !(l->f & P))
	{
		if (l->w > l->len)
			l->ret--;
		ft_putnbr_u_fd(u, 1);
		l->w -= l->len;
		l->len = 0;
	}
}

static void			print_zero_u(t_printf *l, int *count)
{
	while (--l->w > l->len - 1)
	{
		if ((!(l->f & Z) && (l->w > l->p) && !(l->f & P))
		|| (!(l->f & M) && (l->f & P) && (l->f & Z) &&
		(l->p - 1 < l->w)))
			ft_putchar_fd(' ', 1);
		else if (((l->f & Z) && (!(l->f & M))) ||
		(l->w < l->p) || ((l->f & M) && (l->w < l->p)))
			ft_putchar_fd('0', 1);
		else
		{
			if (!(l->f & M))
				ft_putchar_fd(' ', 1);
			else
				*count = *count + 1;
		}
		l->ret++;
	}
}

static void			print_arg_u(t_printf *l, int *count, unsigned int u)
{
	if (*count > 0)
		l->w = *count;
	if (!(l->f & M) || (l->f & P))
	{
		if (!((l->f & P) && (u == 0) &&
		(l->p == 0)))
			ft_putnbr_u_fd(u, 1);
	}
}

static void			print_left_u(t_printf *l, int *count, int *b)
{
	while ((--l->w >= 0) && (l->f & M))
	{
		if (l->w < *count)
		{
			if ((*b == 0) && !(l->f & P))
				l->ret++;
			ft_putchar_fd(' ', 1);
		}
	}
}

void				print_u(t_printf *l)
{
	unsigned int	u;
	int				count;
	int				b;

	b = 0;
	count = 0;
	u = va_arg(l->arg, unsigned int);
	l->len = ft_intlen_u(u);
	l->ret += l->len;
	if (l->w < l->p)
		l->w = l->p;
	if ((u == 0) && (l->p == 0) && (l->f & P))
	{
		l->ret--;
		l->w++;
	}
	print_minus_u(l, u);
	print_zero_u(l, &count);
	print_arg_u(l, &count, u);
	print_left_u(l, &count, &b);
}
