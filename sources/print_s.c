/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_s.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 14:55:09 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/22 14:18:19 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static void	check_precision_s(t_printf *l, int n)
{
	if ((l->f & P) && (n == 0))
	{
		if ((l->p <= l->len) && (l->w >= l->p) && (l->len >= l->w))
			l->len = l->p;
		if ((l->p <= l->len) && (l->w >= l->p) && (l->len <= l->w))
			l->len = l->p;
	}
	else if ((l->f & P) && (n == 1))
	{
		if (l->p < l->len)
			l->len = l->p;
	}
}

static void	print_minus_s(t_printf *l, char **s)
{
	int		i;

	if ((l->f & M))
	{
		i = 0;
		while ((*s)[i] && (((l->f & P) && (i < l->p)) || (!(l->f & P) &&
		(i < l->w + l->len))))
		{
			ft_putchar_fd((*s)[i], 1);
			i++;
			l->ret++;
		}
	}
}

static void	print_space_s(t_printf *l)
{
	while ((--l->w > l->len - 1))
	{
		ft_putchar_fd(' ', 1);
		l->ret++;
	}
}

static void	print_arg_s(t_printf *l, char **s)
{
	int		i;

	if (!(l->f & M))
	{
		i = 0;
		while ((*s)[i])
		{
			if ((l->f & P) && (i >= l->p))
				break ;
			ft_putchar_fd((*s)[i], 1);
			i++;
			l->ret++;
		}
	}
}

void		print_s(t_printf *l)
{
	char	*s;
	int		n;

	n = 0;
	s = va_arg(l->arg, char*);
	if (!s)
	{
		n = 1;
		s = "(null)";
		l->len = 6;
	}
	else
	{
		l->len = ft_strlen(s);
		if (l->len == 0)
			l->p = 0;
	}
	check_precision_s(l, n);
	print_minus_s(l, &s);
	print_space_s(l);
	print_arg_s(l, &s);
}
