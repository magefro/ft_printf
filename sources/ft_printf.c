/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 14:52:33 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/27 15:00:32 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int				ft_printf(const char *frmt, ...)
{
	t_printf	l;

	l.frmt = (char*)frmt;
	va_start(l.arg, frmt);
	l.ret = 0;
	while (*l.frmt != '\0')
	{
		flags_init(&l);
		if (*l.frmt == '%')
		{
			++l.frmt;
			parsing(&l);
		}
		else
		{
			write(1, l.frmt, 1);
			l.ret++;
		}
		l.frmt++;
	}
	va_end(l.arg);
	return (l.ret);
}
