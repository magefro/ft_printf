/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_i.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 15:10:05 by alangloi          #+#    #+#             */
/*   Updated: 2020/11/09 16:17:29 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static void	print_minus_i(t_printf *l, int *nb, int *n)
{
	if (*nb < 0)
	{
		l->ret++;
		if (l->w > l->p)
			l->w--;
	}
	if ((l->f & M) && !(l->f & P))
	{
		if (l->w > l->len)
			l->ret--;
		if (*nb < 0)
			*n = 1;
		if ((*nb == -2147483648) && (l->p > l->len))
			ft_putnbr_u_fd(2147483648, 1);
		else
			ft_putnbr_fd(*nb, 1);
		l->w -= l->len;
		l->len = 0;
	}
}

static void	print_zero_i(t_printf *l, int *n, int *nb, int *count)
{
	while (--l->w > l->len - 1)
	{
		if (((*n == 0) && (*nb < 0) && ((l->f & P) || (l->f & Z)) &&
		(((l->w == l->p - 1) || (l->p > l->w)) || ((l->f & Z) && (*n == 0)
		&& !(l->f & P)))))
		{
			ft_putchar_fd('-', 1);
			*n = 1;
		}
		if ((!(l->f & Z) && (l->w > l->p) && !(l->f & P)) || (!(l->f & M) &&
		(l->f & P) && (l->f & Z) && (l->p - 1 < l->w)))
			ft_putchar_fd(' ', 1);
		else if (((l->f & Z) && (!(l->f & M))) || (l->w < l->p) || ((l->f & M)
		&& (l->w < l->p)))
			ft_putchar_fd('0', 1);
		else
		{
			if (!(l->f & M))
				ft_putchar_fd(' ', 1);
			else
				*count = *count + 1;
		}
		l->ret++;
	}
}

static void	print_arg_i(t_printf *l, int *nb, int *n, int *count)
{
	if (*count > 0)
		l->w = *count;
	if (!(l->f & M) || (l->f & P))
	{
		if ((*nb < 0) && ((l->f & Z) || (l->f & P)) && (*n == 1))
			*nb *= -1;
		if (!((l->f & P) && (*nb == 0) && (l->p == 0)))
		{
			if ((*nb == -2147483648) && (l->p > l->len))
				ft_putnbr_u_fd(2147483648, 1);
			else
				ft_putnbr_fd(*nb, 1);
		}
	}
}

static void	print_left_i(t_printf *l, int *count)
{
	while ((--l->w >= 0) && (l->w < *count))
	{
		if (!(l->f & P) && (!(l->w > 0)))
			l->ret++;
		ft_putchar_fd(' ', 1);
	}
}

void		print_i(t_printf *l)
{
	int		nb;
	int		count;
	int		n;

	n = 0;
	count = 0;
	nb = va_arg(l->arg, int);
	if (nb == -2147483648)
		l->len = 10;
	else
		l->len = ft_intlen(nb);
	l->ret += l->len;
	if (l->w < l->p)
		l->w = l->p;
	if ((nb == 0) && (l->p == 0) && (l->f & P))
	{
		l->ret--;
		l->w++;
	}
	print_minus_i(l, &nb, &n);
	print_zero_i(l, &n, &nb, &count);
	print_arg_i(l, &nb, &n, &count);
	print_left_i(l, &count);
}
