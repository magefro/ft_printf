/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_convert.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/07 16:00:44 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/27 14:25:32 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	check_convert(t_printf *l)
{
	l->conv = *l->frmt;
	if (l->conv == 'c')
		print_c(l);
	if (l->conv == 's')
		print_s(l);
	if (l->conv == 'p')
		print_p(l);
	if (l->conv == 'd')
		print_i(l);
	if (l->conv == 'i')
		print_i(l);
	if (l->conv == 'u')
		print_u(l);
	if (l->conv == 'x')
		print_x(l, 0);
	if (l->conv == 'X')
		print_x(l, 1);
	if (l->conv == '%')
		print_per(l);
}
