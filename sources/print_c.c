/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_c.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 14:51:16 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/22 15:07:49 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void		print_c(t_printf *l)
{
	char	c;

	c = va_arg(l->arg, int);
	if (l->f & M)
	{
		ft_putchar_fd(c, 1);
		l->ret++;
	}
	while (l->w-- > 1)
	{
		ft_putchar_fd(' ', 1);
		l->ret++;
	}
	if (!(l->f & M))
	{
		ft_putchar_fd(c, 1);
		l->ret++;
	}
}
