/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_p.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/03 15:30:24 by alangloi          #+#    #+#             */
/*   Updated: 2020/10/27 14:42:23 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void		print_arg_i(t_printf *l, unsigned long long *p)
{
	ft_putstr_fd("0x", 1);
	if ((p == NULL) && (l->p == 0) && (l->f & P))
		ft_putstr_fd("", 1);
	else
		ft_putstr_fd(ft_utoa_base((unsigned long long)p, 16, 0), 1);
	l->ret += l->len;
}

void		print_p(t_printf *l)
{
	void	*p;

	p = va_arg(l->arg, unsigned long long*);
	if ((p == NULL) && (l->p == 0) && (l->f & P))
	{
		l->w++;
		l->ret--;
	}
	l->len = ft_strlen(ft_utoa_base((unsigned long long)p, 16, 0)) + 2;
	if (l->f & M)
		print_arg_i(l, p);
	while (--l->w > l->len - 1)
	{
		ft_putchar_fd(' ', 1);
		l->ret++;
	}
	if (!(l->f & M))
		print_arg_i(l, p);
}
