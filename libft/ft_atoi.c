/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 17:36:52 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/30 11:54:37 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int				is_neg(const char *s, size_t *i)
{
	unsigned char		*str;

	str = (unsigned char*)s;
	if (str[*i] == '-')
	{
		*i += 1;
		return (1);
	}
	return (0);
}

int						ft_atoi(const char *s)
{
	unsigned char		*str;
	size_t				i;
	size_t				neg;
	size_t				nb;

	i = 0;
	nb = 0;
	neg = 1;
	str = (unsigned char*)s;
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == ' ')
		i++;
	if (is_neg(s, &i))
		neg = -1;
	else if (str[i] == '+')
		i++;
	while (ft_isdigit(str[i]))
	{
		if (neg > 0 && nb > (unsigned int)2147483647)
			return (-1);
		else if (nb > (unsigned int)-2147483648)
			return (0);
		nb = nb * 10 + str[i++] - '0';
	}
	return ((int)(neg * nb));
}
