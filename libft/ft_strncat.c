/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/10 12:59:37 by alangloi          #+#    #+#             */
/*   Updated: 2020/01/02 16:02:20 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char					*ft_strncat(char *s1, const char *s2, size_t n)
{
	unsigned int		s1_len;
	unsigned int		s2_len;

	s1_len = ft_strlen(s1);
	s2_len = ft_strlen(s2);
	if (s2_len < n)
		ft_strcpy(&s1[s1_len], s2);
	else
	{
		ft_strncpy(&s1[s1_len], s2, n);
		s1[s1_len + n] = '\0';
	}
	return (s1);
}
