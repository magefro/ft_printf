/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/07 18:39:22 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/13 23:20:28 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		count_words(char const *s, char c)
{
	int			count;
	int			i;

	i = 0;
	count = 0;
	while (s[i] != '\0')
	{
		while (s[i] == c)
			i++;
		if (s[i] != c && s[i] != '\0')
			count++;
		while (s[i] != c && s[i] != '\0')
			i++;
	}
	return (count);
}

static char		*fill_tab(char const *s, char c, int *j)
{
	char		*str;
	int			k;

	if (!(str = ft_strnew(ft_strlen(s))))
		return (NULL);
	k = 0;
	while (s[*j] != c && s[*j] != '\0')
	{
		str[k] = s[*j];
		k++;
		*j += 1;
	}
	while (s[*j] == c && s[*j] != '\0')
		*j += 1;
	str[k] = '\0';
	return (str);
}

char			**ft_split(char const *s, char c)
{
	int			i;
	int			j;
	int			nb_words;
	char		**tab;

	if (!s)
		return (NULL);
	nb_words = count_words(s, c);
	if (!(tab = (char**)ft_memalloc(sizeof(char*) * nb_words + 1)))
		return (NULL);
	i = 0;
	j = 0;
	while (s[j] != '\0')
	{
		while (s[j] == c && s[j] != '\0')
			j++;
		while (i < nb_words)
		{
			if (!(tab[i] = fill_tab(s, c, &j)))
				ft_memdel((void**)tab);
			i++;
		}
	}
	tab[i] = NULL;
	return (tab);
}
