/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 19:07:31 by alangloi          #+#    #+#             */
/*   Updated: 2020/01/02 21:23:30 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strndup(const char *s1, size_t n)
{
	char		*str;
	size_t		len;

	len = ft_strnlen(s1, n);
	if (!(str = (char*)malloc(len + 1)))
		return (NULL);
	ft_strncpy(str, s1, len);
	return (str);
}
